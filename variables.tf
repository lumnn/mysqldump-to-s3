variable "project_name" {
  type = string
  description = "Base name used for resource naming. The best will be in CamelCase"
}

variable "mysqldump_image" {
  type = string
  default = "mysql:latest"
}

variable "mysql_password_ssm_parameter" {
  type = string
  description = "If you already have password stored in SSM paramteter, then specify it's path here"
}

variable "mysql_password" {
  type = string
  default = ""
  sensitive = true
}

variable "mysqldump_cmd" {
  type = string
  default = "mysqldump --user=\"dbuser\" --password=\"$${MYSQL_PASSWORD}\" --host=\"dbhost\" --port=3306 --set-gtid-purged=OFF \"database_to_dump\""
}

variable "s3_path" {
  type = string
  description = "A S3 file where it will upload dumps. It's the S3 file URL without s3:// prefix"
}

variable "ecs_cluster_arn" {
  type = string
  description = "Cluster arn where scheduled ecs task will run"
}

variable "schedule_expression" {
  type = string
  description = "For example cron(0 12 * * ? *). See: https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html"
}

locals {
  mysql_password_arn = var.mysql_password_ssm_parameter != "" ? var.mysql_password_ssm_parameter : aws_ssm_parameter.mysql_password[0].arn
}
