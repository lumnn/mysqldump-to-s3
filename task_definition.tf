resource "aws_ecs_task_definition" "task_definition" {
  family                   = var.project_name
  execution_role_arn       = aws_iam_role.task_execution_role.arn
  task_role_arn            = aws_iam_role.task_role.arn
  requires_compatibilities = ["EC2"]
  memory                   = 128

  volume {
    name = "shared"

    docker_volume_configuration {
      scope         = "task"
      driver        = "local"
    }
  }

  container_definitions = jsonencode([
    {
      name       = "mysqldump"
      image      = var.mysqldump_image
      essential  = false
      entryPoint = [""]
      command    = [
        "/bin/sh",
        "-c",
        "eval \"$${MYSQLDUMP_CMD}\" > /var/shared/dump.sql"
      ]
      environment = [
        {
          name  = "MYSQLDUMP_CMD",
          value = var.mysqldump_cmd
        }
      ]
      secrets = [{
        name      = "MYSQL_PASSWORD"
        valueFrom = local.mysql_password_arn
      }]
      mountPoints = [{
        sourceVolume = "shared"
        containerPath = "/var/shared"
      }]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group = aws_cloudwatch_log_group.ecs_task.name,
          awslogs-region = data.aws_region.current.name,
          awslogs-stream-prefix = "ecs"
        }
      }
    },
    {
      name       = "awscli"
      image      = "amazon/aws-cli"
      entryPoint = [""]
      command    = [
        "/bin/sh",
        "-c",
        "aws s3 cp /var/shared/dump.sql \"$S3_FILE_URL\""
      ]
      environment = [{
        name   = "S3_FILE_URL"
        value = "s3://${var.s3_path}"
      }]
      dependsOn = [{
        containerName = "mysqldump"
        condition     = "SUCCESS"
      }]
      mountPoints = [{
        sourceVolume  = "shared"
        containerPath = "/var/shared"
      }]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group = aws_cloudwatch_log_group.ecs_task.name,
          awslogs-region = data.aws_region.current.name,
          awslogs-stream-prefix = "ecs"
        }
      }
    }
  ])
}
