resource "aws_cloudwatch_event_rule" "schedule" {
  name = var.project_name
  schedule_expression = var.schedule_expression
}

resource "aws_cloudwatch_event_target" "schedule_event_target" {
  target_id = var.project_name
  rule = aws_cloudwatch_event_rule.schedule.name
  arn = var.ecs_cluster_arn
  role_arn = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/ecsEventsRole"

  ecs_target {
    launch_type = ""
    task_definition_arn = aws_ecs_task_definition.task_definition.arn
  }
}
