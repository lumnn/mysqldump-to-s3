resource "aws_ssm_parameter" "mysql_password" {
  count = var.mysql_password_ssm_parameter != "" ? 0 : 1
  name = "${var.project_name}/mysql_password"
  type = "SecureString"
  value = var.mysql_password
}
